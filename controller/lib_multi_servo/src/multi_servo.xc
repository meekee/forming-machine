/*
 * multi_servo.xc
 *
 *  Created on: May 22, 2015
 *      Author: KB
 */

#include <xs1.h>
#include "multi_servo.h"
#include "ports.h"
#include "shared_ports.h"

static struct servo_param_t {
    unsigned pulses;
    unsigned interval;
    unsigned value;
    unsigned time;
    int position_pulses;
    int delta;
    unsigned direction;
    unsigned home;
    unsigned limit;

} s_param[NUM_SERVO_MAX];

void multi_servo_server(multi_servo_if server servo_if[n], servo_config_t s_config[n], unsigned n,
                        client s_input_port_if p)
{
    timer tmr;
    unsigned pulses_per_sec = 0;
    int pulses = 0;

    /* Initial value for step and direction */
    for(int i = 0; i < n; i++) {
        s_config[i].p_step <: 0;
        s_config[i].p_dir <: 0;
    }

    while(1) {
    #pragma ordered
        select {
        case (int i = 0; i < n; i++)
                (s_param[i].pulses != 0) => tmr when timerafter(s_param[i].time + s_param[i].interval) :> s_param[i].time:

            if(1) {
                if(s_param[i].direction == SERVO_DIR_FORWARD) {

                    if(!s_config[i].end.enable || (s_config[i].end.enable && p.input_bit(s_config[i].end.bit) != 0)) {
                        s_config[i].p_step <: s_param[i].value;
                        s_param[i].value ^= 1;
                        if(s_param[i].value) {
                            s_param[i].position_pulses += s_param[i].delta;
                        }
                        s_param[i].pulses--;
                    } else {
                        s_config[i].p_step <: 0;
                    }

                } else if(s_param[i].direction == SERVO_DIR_REVERSE) {
                    if(!s_config[i].home.enable || (s_config[i].home.enable && p.input_bit(s_config[i].home.bit) != 0)) {
                        s_config[i].p_step <: s_param[i].value;
                        s_param[i].value ^= 1;
                        if(s_param[i].value) {
                            s_param[i].position_pulses += s_param[i].delta;
                        }
                        s_param[i].pulses--;
                    } else {
                        // Reached home
                        s_config[i].p_step <: 0;
                        // Reset positions
                        s_param[i].pulses = 0;
                        s_param[i].position_pulses = 0;
                    }
                }

            } else {
                s_config[i].p_step <: s_param[i].value;
                s_param[i].value ^= 1;
                if(s_param[i].value) {
                    s_param[i].position_pulses += s_param[i].delta;
                }
                s_param[i].pulses--;
            }
            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].pulse(unsigned pulses, unsigned speed_rpm, unsigned direction):
            if(!pulses) {
                break;
            }
            pulses_per_sec = ((speed_rpm * s_config[i].pulses_per_rotation) / 60);
            if(!pulses_per_sec) {
                pulses_per_sec = 1;
            }
            s_param[i].interval = (50000000 / pulses_per_sec);
            s_param[i].value = 1;

            /* Set the direction now */
            s_config[i].p_dir <: direction;
            s_param[i].direction = direction;
            s_param[i].delta = 1?(direction==SERVO_DIR_FORWARD):-1;
            tmr :> s_param[i].time;
            s_param[i].pulses = 2*pulses; // Both ON and OFF

            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].drive(float distance_mm, unsigned speed_mm_s, unsigned direction):
            if(!distance_mm) {
                break;
            }
            pulses_per_sec = (speed_mm_s * s_config[i].pulses_per.mm);
            if(!pulses_per_sec)
            {
                pulses_per_sec = 1;
            }
            s_param[i].interval = (50000000 / pulses_per_sec);
            s_param[i].value = 1;

            /* Set the direction now */
            s_config[i].p_dir <: direction;
            s_param[i].direction = direction;
            s_param[i].delta = 1?(direction==SERVO_DIR_FORWARD):-1;
            tmr :> s_param[i].time;
            s_param[i].pulses = 2*(distance_mm * s_config[i].pulses_per.mm); // Both ON and OFF
            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].rotate(float angle_deg, unsigned speed_deg_s, unsigned direction):
            if(!angle_deg)
            {
                break;
            }
            pulses_per_sec = (speed_deg_s * s_config[i].pulses_per.degree);
            if(!pulses_per_sec)
            {
                pulses_per_sec = 1;
            }
            s_param[i].interval = (50000000 / pulses_per_sec);
            s_param[i].value = 1;

            /* Set the direction now */
            s_config[i].p_dir <: direction;
            s_param[i].direction = direction;
            s_param[i].delta = 1?(direction==SERVO_DIR_FORWARD):-1;
            tmr :> s_param[i].time;
            s_param[i].pulses = 2*(angle_deg * s_config[i].pulses_per.degree); // Both ON and OFF
            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].drive_absolute(float distance_mm, unsigned speed_mm_s):
            pulses_per_sec = (speed_mm_s * s_config[i].pulses_per.mm);
            if(!pulses_per_sec)
            {
                pulses_per_sec = 1;
            }
            s_param[i].interval = (50000000 / pulses_per_sec);
            s_param[i].value = 1;
            pulses = (distance_mm * s_config[i].pulses_per.mm);

            if(pulses > s_param[i].position_pulses) {
                s_param[i].pulses = 2*(pulses - s_param[i].position_pulses); // Both ON and OFF
                s_config[i].p_dir <: SERVO_DIR_FORWARD;
                s_param[i].direction = SERVO_DIR_FORWARD;
                s_param[i].delta = 1;
            } else if(pulses < s_param[i].position_pulses) {
                s_param[i].pulses = 2*(s_param[i].position_pulses - pulses); // Both ON and OFF
                s_config[i].p_dir <: SERVO_DIR_REVERSE;
                s_param[i].direction = SERVO_DIR_REVERSE;
                s_param[i].delta = -1;
            } else {
                /* Do nothing */
            }
            tmr :> s_param[i].time;
            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].rotate_absolute(float angle_deg, unsigned speed_deg_s):
            pulses_per_sec = (speed_deg_s * s_config[i].pulses_per.degree);
            if(!pulses_per_sec)
            {
                pulses_per_sec = 1;
            }
            s_param[i].interval = (50000000 / pulses_per_sec);
            s_param[i].value = 1;
            pulses = (angle_deg * s_config[i].pulses_per.degree);

            if(pulses > s_param[i].position_pulses) {
                s_param[i].pulses = 2*(pulses - s_param[i].position_pulses); // Both ON and OFF
                s_config[i].p_dir <: SERVO_DIR_FORWARD;
                s_param[i].direction = SERVO_DIR_FORWARD;
                s_param[i].delta = 1;
            } else {
                s_param[i].pulses = 2*(s_param[i].position_pulses - pulses); // Both ON and OFF
                s_config[i].p_dir <: SERVO_DIR_REVERSE;
                s_param[i].direction = SERVO_DIR_REVERSE;
                s_param[i].delta = -1;
            }
            tmr :> s_param[i].time;
            break;

        case (int i = 0; i < n; i++)
                (s_param[i].pulses == 0) => servo_if[i].wait() -> unsigned pulses:
            pulses = s_param[i].pulses;
            break;

        case servo_if[int i].position() -> float pos:
            pos = ((float)s_param[i].position_pulses) / s_config[i].pulses_per.mm;
            break;

        case servo_if[int i].reset_position():
            s_param[i].position_pulses = 0;
            s_param[i].pulses = 0;
            break;

        case servo_if[int i].is_home() -> unsigned status:
             if(s_config[i].home.enable) {
                 status = (p.input_bit(s_config[i].home.bit) == 0);
             } else {
                 /* Dont know the status, so better to say No */
                 status = 0;
             }
             break;

        case servo_if[int i].goto_home(unsigned speed_rpm) -> unsigned status:

             char buf[20];
             unsigned pin_value = 0;

             if(s_config[i].home.enable) {
                 pulses_per_sec = ((speed_rpm * s_config[i].pulses_per_rotation) / 60);
                 if(!pulses_per_sec) {
                     pulses_per_sec = 1;
                 }
                 s_param[i].interval = (50000000 / pulses_per_sec);
                 s_config[i].p_dir <: SERVO_DIR_REVERSE;

                 while(p.input_bit(s_config[i].home.bit) != 0) {
                     s_config[i].p_step <: 1;
                     delay_ticks(s_param[i].interval);
                     s_config[i].p_step <: 0;
                     delay_ticks(s_param[i].interval);
                     pin_value = p.input_bit(s_config[i].home.bit);
                 }
                 status = (p.input_bit(s_config[i].home.bit) == 0);

             } else {
                 status = 0;
             }
             break;
        }

    }
}
