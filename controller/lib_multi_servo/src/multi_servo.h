/*
 * multi_servo.h
 *
 *  Created on: May 22, 2015
 *      Author: KB
 */

#include "shared_ports.h"

#ifndef MULTI_SERVO_H_
#define MULTI_SERVO_H_

#define NUM_SERVO_MAX   4

/* Directions */
#define SERVO_DIR_FORWARD       0
#define SERVO_DIR_REVERSE       1

#define SERVO_DIR_CLOCKWISE     0
#define SERVO_DIR_ANTICLOCKWISE 1

typedef enum servo_type {
    SERVO_LINEAR,
    SERVO_ROTARY,
}servo_type_t;

#ifdef __XC__

typedef interface multi_servo_if
{
    [[guarded]] void pulse(unsigned pulses, unsigned speed_rpm, unsigned direction);

    [[guarded]] void drive(float distance_mm, unsigned speed_mm_s, unsigned direction);

    [[guarded]] void rotate(float angle_deg, unsigned degree_s, unsigned direction);

    [[guarded]] void drive_absolute(float distance_mm, unsigned speed_mm_s);

    [[guarded]] void rotate_absolute(float angle_deg, unsigned degree_s);

    [[guarded]] unsigned wait();

    float position();

    void reset_position();

    unsigned is_home();

    unsigned goto_home(unsigned speed_rpm);

} multi_servo_if;

typedef struct limit_input_t
{
    unsigned bit;
    unsigned enable;
} limit_input_t;

/* Servo configuration */
typedef struct servo_config_tag
{
    out port p_step;
    out port p_dir;
    unsigned int pulses_per_rotation;
    servo_type_t type;
    union {
        float mm;
        float degree;
    } pulses_per;
    limit_input_t home;
    limit_input_t end;

} servo_config_t ;


void multi_servo_server(multi_servo_if server servo_if[n], servo_config_t s_configs[n], unsigned n,
                        client s_input_port_if p);

#endif

#endif /* MULTI_SERVO_H_ */
