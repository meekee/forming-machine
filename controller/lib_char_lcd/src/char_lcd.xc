/*
 * char_lcd.xc
 *
 *  Created on: May 31, 2015
 *      Author: KB
 */

#include "char_lcd.h"

#define HIGH    1
#define LOW     0

#define LCD_PORT    p_data, p_rs, p_en

const unsigned char LCD_INIT_ARRAY[4] = {
        0x28,       // 4-bit mode, 2+ lines, 5x8 dots
        0x0C,       // Display ON, No cursor, No blink
        0x01,       // Clear display
        0x06,       // Increment cursor
};

const unsigned char lcd_line_addr[4] = {
        0x00,               // Oth line
        0x40,               // 1st line
        0x14,               // 2nd line
        0x54,               // 3rd line
};

static unsigned lcd_row = 0;

static inline void lcd_send_nibble(port p_data, port p_rs, port p_en,
                                   unsigned char nibble)
{
    p_data <: nibble;
    delay_microseconds(5);
    p_en <: HIGH;
    delay_microseconds(5);
    p_en <: LOW;
    delay_microseconds(5);
}

static void lcd_send_byte(port p_data, port p_rs, port p_en,
                          unsigned char reg, unsigned char byte)
{
    p_rs <: LOW;
    delay_microseconds(60);
    if(reg) {
        p_rs <: HIGH;
        delay_microseconds(5);
    } else {
        p_rs <: LOW;
    }
    delay_microseconds(5);
    p_en <: LOW;
    delay_microseconds(50);
    lcd_send_nibble(p_data, p_rs, p_en, (byte >> 4) & 0x0F);
    lcd_send_nibble(p_data, p_rs, p_en, byte & 0x0F);
}

static void lcd_init(port p_data, port p_rs, port p_en)
{
    p_rs <: LOW;
    delay_milliseconds(10);
    p_en <: LOW;
    lcd_row = 0;

    delay_milliseconds(35);

    for(int i = 0; i < 3; i++) {
        lcd_send_nibble(LCD_PORT, 0x03);
        delay_milliseconds(5);
    }
    lcd_send_nibble(LCD_PORT, 0x02);
    delay_milliseconds(5);

    for(int i = 0; i < sizeof(LCD_INIT_ARRAY); i++) {
        lcd_send_byte(LCD_PORT, 0, LCD_INIT_ARRAY[i]);
        delay_milliseconds(5);
    }
}

static inline void lcd_putchar(port p_data, port p_rs, port p_en, char c)
{
    switch(c) {
    case '\f':
        lcd_send_byte(LCD_PORT, 0, 0x01);
        lcd_row = 0;
        delay_milliseconds(10);
        break;

    case '\n':
        lcd_send_byte(LCD_PORT, 0, 0x80 | lcd_line_addr[++lcd_row]);
        break;

    case '\b':
        lcd_send_byte(LCD_PORT, 0, 0x10);
        break;

    default:
        lcd_send_byte(LCD_PORT, 1, c);
        break;
    }
}

[[distributable]]
void char_lcd_server(server char_lcd_if lcd, port p_data, port p_rs, port p_en)
{
    lcd_init(LCD_PORT);

    while(1)
    {
        select {
            case lcd.gotoxy(int row, int col):
                unsigned addr = lcd_line_addr[row];
                addr += col;
                lcd_send_byte(LCD_PORT, 0, 0x80 | addr);

                break;

            case lcd.lputchar(char c):
                lcd_putchar(LCD_PORT, c);
                break;

            case lcd.write(char data[], int length):
                for(int i = 0; i < length; i++) {
                    lcd_putchar(LCD_PORT, data[i]);
                }
                break;

            case lcd.write_string(char data[]):
                for(int i = 0; data[i] != 0; i++) {
                    lcd_putchar(LCD_PORT, data[i]);
                }
                break;

            case lcd.clear_all():
                lcd_send_byte(LCD_PORT, 0, 0x01);
                delay_milliseconds(10);
                break;
        }
    }
}
