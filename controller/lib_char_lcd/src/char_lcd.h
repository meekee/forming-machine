/*
 * char_lcd.h
 *
 *  Created on: May 31, 2015
 *      Author: KB
 */


#ifndef CHAR_LCD_H_
#define CHAR_LCD_H_

#include <xs1.h>
#include <xccompat.h>
#include <stdio.h>
#include <string.h>

#ifdef __XC__

typedef interface char_lcd_if
{
    void lputchar(char value);

    void gotoxy(int row, int col);

    void write(char data[], int length);

    void write_string(char data[]);

    void clear_all();

} char_lcd_if;

extends client interface char_lcd_if:
{
    inline void clear_row(client interface char_lcd_if self, int row) {

        self.gotoxy(row, 0);
        for(int i = 0; i < 20; i++) {
            self.lputchar(' ');
        }
        self.gotoxy(row, 0);
    }
}

[[distributable]]
void char_lcd_server(server char_lcd_if lcd, port p_data_4bit, port p_rs, port p_en);

#endif

#endif /* CHAR_LCD_H_ */
