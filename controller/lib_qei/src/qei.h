/*
 * qei.h
 *
 *  Created on: May 23, 2015
 *      Author: KB
 */


#ifndef QEI_H_
#define QEI_H_

#include <xs1.h>

#define NUM_QEI_MAX     4

typedef interface qei_if
{
    /* Get QEI count */
    signed int get_count();

    void reset_count();

} qei_if;

[[distributable]]
void qei_handler(server qei_if i_qei[n], unsigned n, streaming chanend c_qei);

void qei_server(streaming chanend c_qei, in port qports[n], clock clk, unsigned n, unsigned digital_filter);

#endif /* QEI_H_ */
