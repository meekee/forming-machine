/*
 * qei.xc
 *
 *  Created on: May 23, 2015
 *      Author: KB
 */

#include <xs1.h>
#include "qei.h"

#define CMD_RESET_COUNT (-1)

/* Lookup table */
// Order is 00 -> 10 -> 11 -> 01
// Bit 3 = Index
static int qei_lookup[4][4] = {
    { 0,  -1,  1,  0 }, // 00
    { 1,  0,  0,  -1 }, // 01
    { -1,  0,  0,  1 }, // 10
    { 0,  1,  -1,  0 }, // 11
  // 00, 01, 10, 11
};

[[distributable]]
void qei_handler(server qei_if i_qei[n], unsigned n, streaming chanend c_qei)
{
    while(1) {
        select {
        case i_qei[int i].get_count() -> signed int count:
            c_qei <: i;
            c_qei :> count;
            break;
        case i_qei[int i].reset_count():
            c_qei <: CMD_RESET_COUNT;
            c_qei <: i;
            break;
        }
    }
}

void qei_server(streaming chanend c_qei, in port qports[n], clock clk, unsigned n, unsigned digital_filter)
{
    int qei_id, ok;
    signed int qei_count[NUM_QEI_MAX] = {0};
    unsigned cur_pin_val[NUM_QEI_MAX] = {0}, old_pin_val[NUM_QEI_MAX] = {0};
    unsigned new_pin_val, new_pin_val_1;

    /* Configure clock */
    configure_clock_ref(clk, 2); // 25 MHz clock

    if(digital_filter > 8) {
        digital_filter = 8;
    }

    for(int j = 0; j < n; j++) {
        /* Set pull ups */
        set_port_pull_down(qports[j]);
        configure_in_port(qports[j], clk);
    }
    start_clock(clk);

    /* Load initial values */
    for(int j = 0; j < n; j++) {
        qports[j] :> cur_pin_val[j];
        old_pin_val[j] = cur_pin_val[j] & 0x03;
    }


    while(1) {
    #pragma ordered
        select {
            case(int id = 0; id < n; id++) qports[id] when pinsneq(cur_pin_val[id]) :> new_pin_val:
                /* Digital filter action: Read the value several times */
                ok = 1;
                #pragma loop unroll
                for(int i = 0; i < digital_filter; i++) {
                    qports[id] :> new_pin_val_1;
                    if(new_pin_val != new_pin_val_1) {
                        ok = 0;
                        break;
                    }
                }
                if(ok) {
                    /* Signal is properly filtered */
                    cur_pin_val[id] = new_pin_val;
                    new_pin_val &= 0x03;
                    qei_count[id] += (qei_lookup[new_pin_val][old_pin_val[id]]);
                    old_pin_val[id] = new_pin_val;
                }

                break;
            case c_qei :> qei_id:
                if(qei_id == CMD_RESET_COUNT) {
                    c_qei :> qei_id;
                    qei_count[qei_id] = 0;

                } else {
                    c_qei <: qei_count[qei_id];
                }
                break;
        }
    }
}
