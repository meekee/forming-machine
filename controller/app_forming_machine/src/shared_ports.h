/*
 * shared_ports.h
 *
 *  Created on: Jul 11, 2015
 *      Author: KB
 */


#ifndef SHARED_PORTS_H_
#define SHARED_PORTS_H_
#include <stdint.h>
#include <stddef.h>

#ifdef __XC__

typedef interface s_input_port_if {

    unsigned input();

    unsigned input_bit(unsigned bit);

}s_input_port_if;

void shared_input_port(server s_input_port_if[n], static const size_t n, in port p);

#endif

#endif /* SHARED_PORTS_H_ */
