/*
 * shared_ports.c
 *
 *  Created on: Jul 11, 2015
 *      Author: KB
 */
#include <xs1.h>
#include <swlock.h>

extern swlock_t port_lock;

unsigned input_p32a(unsigned bit)
{
    unsigned value;
    swlock_acquire(&port_lock);
    asm volatile ("peek %0, res[%1]": "=r"(value): "r"(XS1_PORT_32A));
    swlock_release(&port_lock);
    return ((value >> bit) & 0x01);
}
