/*
 * shared_globals.c
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */
#include "shared_globals.h"
#include "motor_program.h"

#define READ_32(a, id) ((a[id]) | (a[id+1] << 8) | (a[id+2] << 16) | (a[id+3] << 24))
#define READ_16(a, id) ((a[id]) | (a[id+1] << 8))

volatile unsigned g_cb_enable = 0;
volatile run_mode_t g_run_mode = MODE_IDLE;

volatile unsigned g_servo_x_distance = 0;
volatile unsigned g_servo_y_angle = 0;
volatile unsigned g_servo_z_angle = 0;
volatile unsigned g_servo_direction = 0;

volatile unsigned g_mpg_servo_id = 0;

volatile unsigned g_servo_home_req = 0;
volatile unsigned g_home_servo_id = 0;

volatile float g_servo_x_offset = 0;

volatile unsigned g_servo_x_homing_speed_rpm = SERVO_X_DEFAULT_SPEED_RPM;
volatile unsigned g_servo_z_homing_speed_rpm = SERVO_Z_DEFAULT_SPEED_RPM;

volatile unsigned g_servo_x_job_speed = SERVO_X_DEFAULT_SPEED;
volatile unsigned g_servo_y_job_speed = SERVO_Y_DEFAULT_SPEED;

volatile unsigned g_servo_mpg_speed_rpm = SERVO_MPG_SPEED_RPM;
volatile int g_mpg_value_multiplier = 1;

unsigned get_cb_enable()
{
    return g_cb_enable;
}

void set_cb_enable(unsigned value)
{
    g_cb_enable = value;
}

run_mode_t get_run_mode()
{
    return g_run_mode;
}

void set_run_mode(run_mode_t mode)
{
    g_run_mode = mode;
}

unsigned get_mpg_servo_id()
{
    return g_mpg_servo_id;
}

void set_mpg_servo_id(unsigned servo_id)
{
    g_mpg_servo_id = servo_id;
}

unsigned get_servo_angle(unsigned servo_id)
{
    if(servo_id == SERVO_Y) {
        return g_servo_y_angle;
    } else if(servo_id == SERVO_Z) {
        return g_servo_z_angle;
    }
    return 0;

}

void set_servo_angle(unsigned servo_id, unsigned deg)
{
    if(servo_id == SERVO_Y) {
        g_servo_y_angle = deg;
    } else if(servo_id == SERVO_Z) {
        g_servo_z_angle = deg;
    }
}

unsigned get_servo_distance(unsigned servo_id)
{
    if(servo_id == SERVO_X) {
        return g_servo_x_distance;
    }
    return 0;
}

void set_servo_distance(unsigned servo_id, unsigned value)
{
    if(servo_id == SERVO_X) {
        g_servo_x_distance = value;
    }
}

unsigned get_servo_direction(unsigned servo_id)
{
    return g_servo_direction;
}

void set_servo_direction(unsigned servo_id, unsigned direction)
{
    g_servo_direction = direction;
}

unsigned get_home_servo_id()
{
    return g_home_servo_id;
}

void set_home_servo_id(unsigned servo_id)
{
    g_home_servo_id = servo_id;
}

unsigned get_servo_home_req() {
    return g_servo_home_req;
}

void set_servo_home_req(unsigned value) {
    g_servo_home_req = value;
}

unsigned set_parameter(unsigned sub_cmd, unsigned char data[])
{
    unsigned servo_id;

    switch(sub_cmd) {

    case SET_X_OFFSET:
        g_servo_x_offset = ((float)READ_16(data, 0))/10;
        break;

    case SET_HOMING_SPEED_RPM:
        servo_id = data[0];
        if (servo_id == SERVO_X) {
            g_servo_x_homing_speed_rpm = (unsigned)READ_16(data, 1);
        } else if(servo_id == SERVO_Z) {
            g_servo_z_homing_speed_rpm = (unsigned)READ_16(data, 1);
        }
        break;

    case SET_JOB_SPEED:
        servo_id = data[0];
        if (servo_id == SERVO_X) {
            g_servo_x_job_speed = (unsigned)READ_16(data, 1);
        } else if(servo_id == SERVO_Y) {
            g_servo_y_job_speed = (unsigned)READ_16(data, 1);
        }
        break;

    case SET_MPG_SPEED:
        g_servo_mpg_speed_rpm = (unsigned)READ_16(data, 0);
        break;

    case SET_MPG_MULTIPLIER:
        g_mpg_value_multiplier = (int)READ_16(data, 0);
        break;

    default:
        // Invalid parameter
        return 0;
    }

    return 1;
}

// Different parameter get functions
float get_servo_x_offset()
{
    return g_servo_x_offset;
}

unsigned get_servo_x_homing_speed_rpm()
{
    return g_servo_x_homing_speed_rpm;
}

unsigned get_servo_z_homing_speed_rpm()
{
    return g_servo_z_homing_speed_rpm;
}

unsigned get_servo_x_job_speed()
{
    return g_servo_x_job_speed;
}

unsigned get_servo_y_job_speed()
{
    return g_servo_y_job_speed;
}

unsigned get_servo_mpg_speed_rpm()
{
    return g_servo_mpg_speed_rpm;
}

int get_mpg_value_multiplier()
{
    return g_mpg_value_multiplier;
}
