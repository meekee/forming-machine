
#include <string.h>
#include <print.h>
#include "uart_handler.h"
#include "motor_program.h"
#include "cb_manager.h"


/* Communication with Pi */
#define U_SOF 0xAE
#define U_EOF 0xEA

// Commands
#define CMD_PING                 0x80
#define CMD_ENABLE_CTRL_BOARD    0x81
#define CMD_DISABLE_CTRL_BOARD   0x82

#define CMD_MODE_PROGRAM         0x83
#define CMD_MODE_MPG             0x84
#define CMD_LOAD_PROGRAM         0x85

#define CMD_GET_ANGLE            0x86
#define CMD_GET_DISTANCE         0x87

#define CMD_MODE_IDLE            0x88
#define CMD_GOTO_HOME            0x89

#define CMD_SET_PARAMETER        0x8A

// Delimiter - used to determine the packet type (CMD or RSP)
#define CMD_RSP_DELIMITER 0xBF

// Responses
#define RSP_PING     0xC0
#define RSP_OK       0xC1
#define RSP_ERROR    0xC2
#define RSP_UNKNOWN  0xD0

// Sub-commands for set parameter
#define SUB_CMD_SET_X_OFFSET                0x40
#define SUB_CMD_SET_HOMING_SPEED_RPM        0x41
#define SUB_CMD_SET_JOB_SPEED               0x42
#define SUB_CMD_SET_MPG_SPEED               0x43
#define SUB_CMD_SET_MPG_MULTIPLIER          0x44


// Others
#define MAX_PACKET_LEN (10 + (10 * MAX_PROGRAM_STEPS))

/* Command handlers can be moved to different file */

/* Blocking-wait to read command
 * Returns the length
 */
unsigned read_command(client uart_rx_if uart_rx, unsigned char cmd_buf[])
{
    unsigned char data;
    unsigned length, count, tmp;
    //printstrln("Data:");
    // Read SOF first
    data = uart_rx.wait_for_data_and_read();
    //printhexln(data);
    if(U_SOF != data) {
        return 0;
    }

    // Read length now - (2 bytes)
    data = uart_rx.wait_for_data_and_read();
    length = data; // LSB
    data = uart_rx.wait_for_data_and_read();
    tmp = data;
    length |= (tmp << 8);
    //printhexln(data);
    if(length > MAX_PACKET_LEN) {
        // error
        return 0;
    }

    count = length;
    // Read Command now
    unsigned i = 0;
    while(count) {
        cmd_buf[i++] = uart_rx.wait_for_data_and_read();
        count--;
    }

    // check EOF now
    data = uart_rx.wait_for_data_and_read();
    if(data != U_EOF) {
        // Error no EOF found!
        return 0;
    }

    return length;
}

void send_response(client uart_tx_if uart_tx, unsigned char rsp_array[], unsigned rsp_len)
{
   static unsigned char array[25];
   array[0] = U_SOF;
   array[1] = (rsp_len & 0xFF);
   array[2] = ((rsp_len >> 8) & 0xFF);
   memcpy(array+3, rsp_array, rsp_len);
   array[rsp_len + 3] = U_EOF;
   for(int i = 0; i < (rsp_len + 4); i++) {
       uart_tx.write(array[i]);
   }
}

void handle_command(client uart_tx_if uart_tx, unsigned char cmd[])
{
    unsigned char rsp[12];
    unsigned p_len = 0;

//    for(int i = 0; i < 20; i++) {
//        debug_printf("%x ",cmd[i]);
//    }
//    debug_printf("\n");

    switch(cmd[0]) {
        case CMD_PING:
            rsp[0] = RSP_PING;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_MODE_IDLE:
            set_run_mode(MODE_IDLE);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_MODE_PROGRAM:
            set_run_mode(MODE_PROGRAM);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_LOAD_PROGRAM:
            p_len = cmd[1];
            if(p_len) {
                if(store_motor_program(cmd+2, p_len)) {
                    /* Success */
                    rsp[0] = RSP_OK;
                    send_response(uart_tx, rsp, 1);
                    break;
                }
            }
            rsp[0] = RSP_ERROR;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_MODE_MPG:
            set_mpg_servo_id(cmd[1]);
            set_run_mode(MODE_MPG);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_GET_ANGLE:
            if(get_run_mode() == MODE_MPG) {
                unsigned angle_deg;
                angle_deg = get_servo_angle(cmd[1]);
                rsp[0] = RSP_OK;
                rsp[1] = angle_deg & 0xFF;
                rsp[2] = (angle_deg >> 8) & 0xFF;
                rsp[3] = (get_servo_direction(cmd[1]) & 0xFF);
                send_response(uart_tx, rsp, 4);
            } else {
                rsp[0] = RSP_ERROR;
                send_response(uart_tx, rsp, 1);
            }
            break;

        case CMD_GET_DISTANCE:
            if(get_run_mode() == MODE_MPG) {
                unsigned dist_mm;
                dist_mm = get_servo_distance(cmd[1]);
                rsp[0] = RSP_OK;
                rsp[1] = dist_mm & 0xFF;
                rsp[2] = (dist_mm >> 8) & 0xFF;
                rsp[3] = (get_servo_direction(cmd[1]) & 0xFF);
                send_response(uart_tx, rsp, 4);
            } else {
                rsp[0] = RSP_ERROR;
                send_response(uart_tx, rsp, 1);
            }
            break;

        case CMD_GOTO_HOME:
            set_home_servo_id(cmd[1]);
            set_servo_home_req(1);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_SET_PARAMETER:
            if(set_parameter(cmd[1], cmd+2)) {
                rsp[0] = RSP_OK;
                send_response(uart_tx, rsp, 1);
                break;
            }
            rsp[0] = RSP_ERROR;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_ENABLE_CTRL_BOARD:
            set_cb_enable(1);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

        case CMD_DISABLE_CTRL_BOARD:
            set_cb_enable(0);
            rsp[0] = RSP_OK;
            send_response(uart_tx, rsp, 1);
            break;

    }

}

void uart_handler(client uart_tx_if uart_tx, client uart_rx_if uart_rx)
{
    unsigned len;
    unsigned char cmd_buf[MAX_PACKET_LEN];

    while(1)
    {
        len = read_command(uart_rx, cmd_buf);
        if(len) {
            handle_command(uart_tx, cmd_buf);
        }
    }

}
