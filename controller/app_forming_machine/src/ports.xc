/*
 * ports.xc
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */
#include <xs1.h>
#include <platform.h>
#include <swlock.h>

swlock_t port_lock = SWLOCK_INITIAL_VALUE;

// Definition of all ports goes here

in port p_in_32a = XS1_PORT_32A;

/* UART port pins */
port p_uart_rx = on tile[0]: XS1_PORT_1H;
port p_uart_tx = on tile[0]: XS1_PORT_1F;

/* QEI port pins */
in port p_qei[1] = {on tile[0]: XS1_PORT_4C};

/* Servo ON/OFF */
out port p_servo_on_off = on tile[0]: XS1_PORT_4A;

/* Misc port */
out port p_4b = on tile[0]: XS1_PORT_4B;

/* LCD ports */
port p_data = XS1_PORT_4D;
port p_rs   = XS1_PORT_1O;
port p_en   = XS1_PORT_1P;


int input_bit(in port p, unsigned bit)
{
    int value;
    //swlock_acquire(port_lock);
    //value = peek(p);
    p :> value;
    //swlock_release(port_lock);
    return ((value >> bit) & 0x01);
}

void output_bit(out port p, unsigned bit, unsigned value)
{
    unsigned tmp;
    tmp = peek(p);
    if(value) {
        tmp |= (1 << bit);
    } else {
        tmp &= (~(1 << bit));
    }
    p <: tmp;
    delay_ticks(25);
}
