/*
 * ports.h
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */


#ifndef PORTS_H_
#define PORTS_H_

#include <xs1.h>
#include <platform.h>

/* I/O function APIs */

void output_bit(out port p, unsigned bit, unsigned value);

int input_bit(in port p, unsigned bit);

unsigned input_p32a(unsigned bit);

/* Motor control ports
 * Servo X = Ball Screw Motor
 * Servo Y = Holding Motor
 * Servo Z = Bending Gear Motor */
#define PORT_SERVO_X_STEP   XS1_PORT_1J
#define PORT_SERVO_X_DIR    XS1_PORT_1K

#define PORT_SERVO_Y_STEP   XS1_PORT_1G
#define PORT_SERVO_Y_DIR    XS1_PORT_1E

#define PORT_SERVO_Z_STEP   XS1_PORT_1M
#define PORT_SERVO_Z_DIR    XS1_PORT_1N

extern in port p_in_32a;

extern port p_uart_rx;
extern port p_uart_tx;

extern in port p_qei[1];

extern out port p_servo_on_off;

extern out port p_4b;

extern port p_data;
extern port p_rs;
extern port p_en;

/* Unshared Pin definitions */
//#define PIN_START       p_in_32a, 8
#define PIN_SERVO_X_ON  p_servo_on_off, 1
#define PIN_SERVO_Y_ON  p_servo_on_off, 0
#define PIN_SERVO_Z_ON  p_servo_on_off, 2

//#define PIN_SERVO_X_HOME    p_in_32a, 10
//#define PIN_SERVO_X_LIMIT   p_in_32a, 9
//#define PIN_SERVO_Z_HOME    p_in_32a, 18

/* Shared with servo ON/OFF port */
#define PIN_ALARM       p_servo_on_off, 3

#define PIN_HEART_BEAT              p_4b, 0
#define PIN_GRIPPER_SOLENOID_CLOSE  p_4b, 1
#define PIN_GRIPPER_SOLENOID_OPEN   p_4b, 2
#define PIN_SOLENOID_CONTROL        p_4b, 3

//#define PIN_SERV0_Y_C_PHASE         p_in_32a, 1
//#define PORT_C_PHASE                p_in_32a
//#define BIT_C_PHASE                 1
//#define PIN_F1                      p_in_32a, 14
//#define PIN_F2                      p_in_32a, 13
//#define PIN_REED_SWITCH             p_in_32a, 5

/* Shared pin definitions and they all start with S_
 * XS1_PORT_32A is shared among different cores */
#define S_PIN_START             8
//#define S_PIN_SERVO_X_HOME      10
#define S_PIN_SERVO_X_HOME      6
#define S_PIN_SERVO_X_LIMIT     9
#define S_PIN_SERVO_Z_HOME      18
#define S_PIN_SERVO_Z_LIMIT     5
#define S_PIN_F1                14
#define S_PIN_F2                13
#define S_PIN_SERV0_Y_C_PHASE   1
#define S_PIN_REED_SWITCH       5

#endif /* PORTS_H_ */
