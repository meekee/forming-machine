/*
 * shared_ports.xc
 *
 *  Created on: Jul 11, 2015
 *      Author: KB
 */

#include "shared_ports.h"

void shared_input_port(server s_input_port_if port_if[n], static const size_t n, in port p)
{
    while(1) {

        select {
            case port_if[int i].input() -> unsigned value:
                p :> value;
                break;

            case port_if[int i].input_bit(unsigned bit) -> unsigned value:
                p :> value;
                value = (value >> bit);
                value &= 0x01;
                break;
        }
    }
}
