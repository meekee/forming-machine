/*
 * shared_globals.h
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */


#ifndef SHARED_GLOBALS_H_
#define SHARED_GLOBALS_H_

#include <debug_print.h>

// Set parameter sub commands
#define SET_X_OFFSET                0x40
#define SET_HOMING_SPEED_RPM        0x41
#define SET_JOB_SPEED               0x42
#define SET_MPG_SPEED               0x43
#define SET_MPG_MULTIPLIER          0x44

typedef enum run_mode_tag {
    MODE_PROGRAM,
    MODE_MPG,
    MODE_IDLE,
} run_mode_t;

void set_run_mode(run_mode_t mode);
run_mode_t get_run_mode();

unsigned get_cb_enable();
void set_cb_enable(unsigned value);

unsigned get_servo_angle(unsigned servo_id);
void set_servo_angle(unsigned servo_id, unsigned deg);

unsigned get_servo_distance(unsigned servo_id);
void set_servo_distance(unsigned servo_id, unsigned value);

unsigned get_servo_direction(unsigned servo_id);
void set_servo_direction(unsigned servo_id, unsigned direction);

unsigned get_mpg_servo_id();
void set_mpg_servo_id(unsigned servo_id);

unsigned get_home_servo_id();
void set_home_servo_id(unsigned servo_id);

unsigned get_servo_home_req();
void set_servo_home_req(unsigned value);

unsigned set_parameter(unsigned sub_cmd, unsigned char data[]);
float get_servo_x_offset();

unsigned get_servo_x_homing_speed_rpm();
void set_servo_x_homing_speed_rpm(unsigned speed_rpm);

unsigned get_servo_z_homing_speed_rpm();
void set_servo_z_homing_speed_rpm(unsigned speed_rpm);

unsigned get_servo_x_job_speed();
void set_servo_x_job_speed(unsigned speed_mm_s);

unsigned get_servo_y_job_speed();
void set_servo_y_job_speed(unsigned speed_deg_s);

unsigned get_servo_mpg_speed_rpm();
void set_servo_mpg_speed_rpm(unsigned speed_rpm);

int get_mpg_value_multiplier();
void set_mpg_value_multiplier(int x);

#endif /* SHARED_GLOBALS_H_ */
