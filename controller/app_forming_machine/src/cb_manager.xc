/*
 * cb_manager.xc
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */
#include <xs1.h>
#include <timer.h>
#include <multi_servo.h>
#include <print.h>

#include <qei.h>
#include <char_lcd.h>
#include "ports.h"
#include "motor_program.h"
#include "shared_globals.h"

void servo_goto_home(client interface multi_servo_if servo[3], unsigned servo_id,
                     client char_lcd_if ?lcd, client s_input_port_if p32a)
{
    unsigned all = 0;

    if (servo_id == SERVO_ALL) {
        all = 1;
    }

    if(all || servo_id == SERVO_X) {
        delay_milliseconds(100);
        /* Get Servo X to home */
        servo[SERVO_X].goto_home(get_servo_x_homing_speed_rpm());
    }

    if(all || servo_id == SERVO_Z) {
        delay_milliseconds(100);
        /* Get Servo Z to home */
        servo[SERVO_Z].goto_home(get_servo_z_homing_speed_rpm());
    }

    if(all || servo_id == SERVO_X) {
        servo[SERVO_X].reset_position();
    }
    if(all || servo_id == SERVO_Y) {
        servo[SERVO_Y].reset_position();
    }
    if(all || servo_id == SERVO_Z) {
        servo[SERVO_Z].reset_position();
    }
}

void do_program(client interface multi_servo_if servo[3], client char_lcd_if ?lcd, client s_input_port_if p32a) {

    unsigned deg_per_sec = 0;
    char buf[20];

    if(!isnull(lcd)) {
        lcd.clear_all();
        lcd.gotoxy(0,0);
        lcd.write_string("Servo: ");
        lcd.gotoxy(1,0);
        lcd.write_string("Units: ");
    }

    //drive_hobby_servo(RIGHT, 2000);

    unsafe {
        if(g_forming_program.state == PROGRAM_ACTIVE) {

            if(!isnull(lcd)) {
                lcd.gotoxy(0, 8);
                lcd.write_string("          ");
                lcd.gotoxy(0, 8);
                lcd.write_string("X");
                lcd.gotoxy(1, 8);
                sprintf(buf, "%5f", get_servo_x_offset());
                lcd.write_string(buf);
            }

            /* Move X to offset */
            servo[SERVO_X].drive_absolute(get_servo_x_offset(), get_servo_x_homing_speed_rpm());
            /* Wait till we reach offset */
            servo[SERVO_X].wait();

            delay_milliseconds(100);
            if(!isnull(lcd)) {
                /* Move Y to Home */
                lcd.gotoxy(0, 8);
                lcd.write_string("          ");
                lcd.gotoxy(0, 8);
                lcd.write_string("Y Homing");
                lcd.gotoxy(1, 8);
                lcd.write_string("0         ");

                //servo_goto_home(servo, SERVO_Y);
                /* Move Z to Home */
                lcd.gotoxy(0, 8);
                lcd.write_string("          ");
                lcd.gotoxy(0, 8);
                lcd.write_string("Z Homing");
                lcd.gotoxy(1, 8);
                lcd.write_string("0         ");
            }

            servo_goto_home(servo, SERVO_Z, lcd, p32a);


            for(int i = 0; i < g_forming_program.step_count; i++) {
                /* Run X servo */
                if(!isnull(lcd)) {
                    lcd.gotoxy(0, 8);
                    lcd.write_string("          ");
                    lcd.gotoxy(0, 8);
                    lcd.write_string("X");
                    lcd.gotoxy(1, 8);
                    sprintf(buf, "%5f", g_forming_program.step[i].x_distance_mm);
                    lcd.write_string(buf);
                }

                /* Always drive X with Offset */
                servo[SERVO_X].drive_absolute(get_servo_x_offset() + g_forming_program.step[i].x_distance_mm,
                                              get_servo_x_job_speed());
                servo[SERVO_X].wait();
                delay_milliseconds(50);

                if(!isnull(lcd)) {
                    lcd.gotoxy(0, 8);
                    lcd.write_string("          ");
                    lcd.gotoxy(0, 8);
                    lcd.write_string("Y");
                    lcd.gotoxy(1, 8);
                    sprintf(buf, "%5f", g_forming_program.step[i].y_rotate_deg);
                    lcd.write_string(buf);
                }

                servo[SERVO_Y].rotate_absolute(g_forming_program.step[i].y_rotate_deg,
                                               get_servo_y_job_speed());

                /* Wait for completion of the servo Y */
                servo[SERVO_Y].wait();
                delay_milliseconds(50);

                if(!isnull(lcd)) {
                    lcd.gotoxy(0, 8);
                    lcd.write_string("          ");
                    lcd.gotoxy(0, 8);
                    lcd.write_string("Z");
                    lcd.gotoxy(1, 8);
                    sprintf(buf, "%5f", g_forming_program.step[i].z_rotate_deg);
                    lcd.write_string(buf);
                }


                deg_per_sec = g_forming_program.step[i].z_speed_rpm * 6; // 360/60
                servo[SERVO_Z].rotate_absolute(g_forming_program.step[i].z_rotate_deg,
                                deg_per_sec);
                /* Wait for completion of the servo Z */
                servo[SERVO_Z].wait();
                delay_milliseconds(50);
            }

        } // if(g_forming_program.state == PROGRAM_ACTIVE)

        //drive_hobby_servo(LEFT, 2000);

        if(!isnull(lcd)) {
            lcd.gotoxy(0, 8);
            lcd.write_string("          ");
            lcd.gotoxy(1, 8);
            lcd.write_string("          ");
        }
    }
}

void servo_all_on(void)
{
    /* Switch ON servos */
    output_bit(PIN_SERVO_X_ON, 1);
    delay_milliseconds(500);
    output_bit(PIN_SERVO_Y_ON, 1);
    delay_milliseconds(500);
    output_bit(PIN_SERVO_Z_ON, 1);
    delay_milliseconds(500);

}

void servo_all_off(void)
{
    /* Switch OFF servos */
    output_bit(PIN_SERVO_X_ON, 0);
    delay_milliseconds(500);
    output_bit(PIN_SERVO_Y_ON, 0);
    delay_milliseconds(500);
    output_bit(PIN_SERVO_Z_ON, 0);
    delay_milliseconds(500);
}

void control_board_task(client multi_servo_if servo[3], client qei_if qei[1], client char_lcd_if ?lcd, client s_input_port_if p32a)
{
    run_mode_t mode = MODE_PROGRAM;
    signed int mpg_pulses = 0;
    signed int motor_pulses = 0;
    signed int diff;
    unsigned servo_id = 0;
    char buf[20];

    /* Do all initializations here */
    delay_seconds(2);
    servo_all_on();
    delay_seconds(3);

    //debug_printf("Homing...\n");
    /* Get all motors to their home positions */
    servo_goto_home(servo, SERVO_ALL, lcd, p32a);

    //debug_printf("Start...\n");

    /* Infinite loop */
    while(1) {
        if(get_cb_enable()) {
            //debug_printf("M=%x\r",mode);
            if(get_run_mode() == MODE_PROGRAM) {
                if(mode != MODE_PROGRAM) {
                    /* Do any required mode initializations here */
                    mode = MODE_PROGRAM;
                }

                if(p32a.input_bit(S_PIN_START) == 0) {
                    //printcharln('C');
                    delay_milliseconds(10);  // Debounce delay
                    /* Wait for pin to become High */
                    while(p32a.input_bit(S_PIN_START) == 0);

                    /* Start pin is pressed */
                    do_program(servo, lcd, p32a);
                }

                //
                if(get_servo_home_req()) {
                    servo_goto_home(servo, get_home_servo_id(), lcd, p32a);
                    set_servo_home_req(0);
                }

            }
            else if(get_run_mode() == MODE_MPG) {
                if(mode != MODE_MPG) {
                    /* Entering MPG mode, do any required initializations */
                    mpg_pulses = 0;
                    motor_pulses = 0;
                    servo_id = get_mpg_servo_id();
                    set_servo_distance(servo_id, 0);
                    set_servo_angle(servo_id, 0);
                    set_servo_direction(servo_id, 0);
                    qei[0].reset_count();

                    //debug_printf("MPG-%x\n",servo_id);
                    if(!isnull(lcd)) {
                        lcd.gotoxy(0,0);
                        lcd.write_string("DIFF: ");
                        lcd.gotoxy(1,0);
                        lcd.write_string("MPG:  ");
                        lcd.gotoxy(2,0);
                        lcd.write_string("MOT:  ");
                    }

                    mode = MODE_MPG;
                }
                /* Read QEI */
                mpg_pulses = get_mpg_value_multiplier() * qei[0].get_count();

                diff = mpg_pulses - motor_pulses;

                if(diff > 0) {
                    /* Forward */
                    servo[servo_id].pulse((unsigned)diff, get_servo_mpg_speed_rpm(), SERVO_DIR_FORWARD);
                } else if(diff < 0) {
                    /* Reverse */
                    servo[servo_id].pulse((unsigned)-diff, get_servo_mpg_speed_rpm(), SERVO_DIR_REVERSE);
                }
                motor_pulses += diff;

                if(!isnull(lcd)) {
                    lcd.gotoxy(0,5);
                    sprintf(buf,"%10d",diff);
                    lcd.write_string(buf);
                    lcd.gotoxy(1,5);
                    sprintf(buf,"%10d",mpg_pulses);
                    lcd.write_string(buf);
                    lcd.gotoxy(2,5);
                    sprintf(buf,"%10d",motor_pulses);
                    lcd.write_string(buf);
                    delay_microseconds(50);
                    diff = 0;
                }

                /* Update the motor pulses */
                if(servo_id == SERVO_X) {
                    if(motor_pulses < 0) {
                        set_servo_distance(servo_id, ((-motor_pulses)*10/SERVO_X_PULSES_PER_MM));
                        set_servo_direction(servo_id, SERVO_DIR_REVERSE);
                    } else {
                        set_servo_distance(servo_id, (motor_pulses*10/SERVO_X_PULSES_PER_MM));
                        set_servo_direction(servo_id, SERVO_DIR_FORWARD);
                    }
                } else if(servo_id == SERVO_Y) {
                    if(motor_pulses < 0) {
                        set_servo_angle(servo_id, ((-motor_pulses)*10/SERVO_Y_PULSES_PER_DEG));
                        set_servo_direction(servo_id, SERVO_DIR_ANTICLOCKWISE);
                    } else {
                        set_servo_angle(servo_id, (motor_pulses*10/SERVO_Y_PULSES_PER_DEG));
                        set_servo_direction(servo_id, SERVO_DIR_CLOCKWISE);
                    }
                } else if(servo_id == SERVO_Z) {
                    if(motor_pulses < 0) {
                        set_servo_angle(servo_id, ((-motor_pulses)*10/SERVO_Z_PULSES_PER_DEG));
                        set_servo_direction(servo_id, SERVO_DIR_ANTICLOCKWISE);
                    } else {
                        set_servo_angle(servo_id, (motor_pulses*10/SERVO_Z_PULSES_PER_DEG));
                        set_servo_direction(servo_id, SERVO_DIR_CLOCKWISE);
                    }
                }
                /* Drive requested servo */
            } else if(get_run_mode() == MODE_IDLE) {
                mode = MODE_IDLE;
            }

            /* Check F1 and F2 */
            if(p32a.input_bit(S_PIN_F1) == 0) {
                delay_milliseconds(10);
                while(p32a.input_bit(S_PIN_F1) == 0);
                servo_all_off();

            }
            if(p32a.input_bit(S_PIN_F2) == 0) {
                delay_milliseconds(10);
                while(p32a.input_bit(S_PIN_F2) == 0);
                servo_all_on();

            }
        }

    }

}
