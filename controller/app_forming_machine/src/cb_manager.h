/*
 * cb_manager.h
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */


#ifndef CB_MANAGER_H_
#define CB_MANAGER_H_

#include <qei.h>
#include <char_lcd.h>
#include <multi_servo.h>
#include "shared_globals.h"

void control_board_task(client multi_servo_if servo[3], client qei_if qei[1], client char_lcd_if ?lcd, client s_input_port_if p32a);

#endif /* CB_MANAGER_H_ */
