/*
 * uart_handler.h
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */


#ifndef UART_HANDLER_H_
#define UART_HANDLER_H_

#include <uart.h>

#define BAUD_RATE 115200
#define RX_BUFFER_SIZE 64

void uart_handler(client uart_tx_if uart_tx, client uart_rx_if uart_rx);

#endif /* UART_HANDLER_H_ */
