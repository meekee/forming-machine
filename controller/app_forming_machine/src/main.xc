/*
 * app_forming_machine.xc
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */



#include <xs1.h>
#include <platform.h>
#include <uart.h>
#include <multi_servo.h>
#include <qei.h>
#include <gpio.h>

#include "motor_program.h"
#include "shared_ports.h"
#include "ports.h"
#include "uart_handler.h"
#include "cb_manager.h"


servo_config_t c_servo[3] = {
        {PORT_SERVO_X_STEP, PORT_SERVO_X_DIR,
         SERVO_X_PULSES_PER_ROTATION, SERVO_LINEAR, {SERVO_X_PULSES_PER_MM},
        {S_PIN_SERVO_X_HOME, 1}, {S_PIN_SERVO_X_LIMIT, 1}},    // Ball Screw Motor
        {PORT_SERVO_Y_STEP, PORT_SERVO_Y_DIR,
         SERVO_Y_PULSES_PER_ROTATION, SERVO_ROTARY, {SERVO_Y_PULSES_PER_DEG},
        {0, 0}, {0, 0}},    // Holding Motor
        {PORT_SERVO_Z_STEP, PORT_SERVO_Z_DIR,
         SERVO_Z_PULSES_PER_ROTATION, SERVO_ROTARY, {SERVO_Z_PULSES_PER_DEG},
        {S_PIN_SERVO_Z_HOME, 1}, {S_PIN_SERVO_Z_LIMIT, 0}},   // Bending Gear Motor
};

clock clk_qei = XS1_CLKBLK_1;

char p32_pin_map[16] = {0, 4, 5, 6,
                        8, 9, 10,
                        13, 14, 15, 16, 17, 18, 19,
                        10, 9
                        };

int main(void)
{
    interface uart_rx_if i_rx;
    interface uart_tx_if i_tx;
    input_gpio_if i_gpio_rx;
    output_gpio_if i_gpio_tx[1];
    streaming chan c_qei;
    qei_if qei[1];
    interface char_lcd_if lcd;
    interface multi_servo_if servo_if[3];
    interface s_input_port_if port_32a_if[2];

    par
    {
        /* Main task handler */
        on tile[0]: control_board_task(servo_if, qei, lcd, port_32a_if[0]);

        /* Multi-servo */
        on tile[0]: multi_servo_server(servo_if, c_servo, 3, port_32a_if[1]);

        on tile[0]: [[distribute]] qei_handler(qei, 1, c_qei);

        on tile[0]: qei_server(c_qei, p_qei, clk_qei, 1, 4);

        on tile[0]: [[distribute]] char_lcd_server(lcd, p_data, p_rs, p_en);

        on tile[0]: shared_input_port(port_32a_if, 2, p_in_32a);

        /* Custome UART handler */
        on tile[0]: uart_handler(i_tx, i_rx);

        /* UART library */
        on tile[0]: output_gpio(i_gpio_tx, 1, p_uart_tx, null);
        on tile[0]: uart_tx(i_tx, null,
                            BAUD_RATE, UART_PARITY_NONE, 8, 1,
                            i_gpio_tx[0]);

        on tile[0].core[0] : input_gpio_1bit_with_events(i_gpio_rx, p_uart_rx);
        on tile[0].core[0] : uart_rx(i_rx, null, RX_BUFFER_SIZE,
                                     BAUD_RATE, UART_PARITY_NONE, 8, 1,
                                     i_gpio_rx);
    }
    return 0;
}
