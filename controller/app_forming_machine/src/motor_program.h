/*
 * motor_program.h
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */


#ifndef MOTOR_PROGRAM_H_
#define MOTOR_PROGRAM_H_

/* Servo IDs */
#define SERVO_X     0
#define SERVO_Y     1
#define SERVO_Z     2
#define SERVO_ALL   0xF

// Motor defines
#define SERVO_X_DEFAULT_SPEED   20   // 5mm/s
#define SERVO_Y_DEFAULT_SPEED   45  // 10 degree/s
#define SERVO_Z_DEFAULT_SPEED   45  // 10 degree/s

#define SERVO_X_DEFAULT_SPEED_RPM  20 // 20 RPM
#define SERVO_Y_DEFAULT_SPEED_RPM  2  // 3 RPM
#define SERVO_Z_DEFAULT_SPEED_RPM  20 // 20 RPM

#define SERVO_X_OFFSET_SPEED_RPM        20 // 20 RPM
#define SERVO_X_JOB_DETECT_SPEED_RPM    5  // 5 RPM

/* MPG servo speed in RPM */
#define SERVO_MPG_SPEED_RPM     100

/* Servo pulses per rotation defines */
#define SERVO_X_PULSES_PER_ROTATION     1000
#define SERVO_Y_PULSES_PER_ROTATION     3600
#define SERVO_Z_PULSES_PER_ROTATION     3600

#define SERVO_X_PULSES_PER_MM   100
#define SERVO_Y_PULSES_PER_DEG  10
#define SERVO_Z_PULSES_PER_DEG  10

#define MAX_PROGRAM_STEPS   200

#define PROGRAM_ACTIVE      0xAA
#define PROGRAM_IDLE        0x00

typedef struct program_step_t
{
    float x_distance_mm;
    unsigned x_direction;
    float y_rotate_deg;
    unsigned y_direction;
    float z_rotate_deg;
    unsigned z_direction;
    unsigned z_speed_rpm;

} program_step_t;

typedef struct motor_program_t
{
    program_step_t step[MAX_PROGRAM_STEPS];
    unsigned step_count;
    unsigned state;

} motor_program_t;

extern motor_program_t g_forming_program;

//typedef interface motor_program_if {
//
//    void store(unsigned char prog_buf[]);
//
//    void get(unsigned char prog_buf[]);
//
//} motor_program_if;

int store_motor_program(unsigned char prog_buf[], unsigned len);

#endif /* MOTOR_PROGRAM_H_ */
