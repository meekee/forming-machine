/*
 * motor_program.xc
 *
 *  Created on: Jun 15, 2015
 *      Author: KB
 */

#include "multi_servo.h"
#include "motor_program.h"

#define READ_32(a, id) ((a[id]) | (a[id+1] << 8) | (a[id+2] << 16) | (a[id+3] << 24))
#define READ_16(a, id) ((a[id]) | (a[id+1] << 8))

/* Global parameter to hold the program */
motor_program_t g_forming_program = {
        .step = {
                {30,SERVO_DIR_FORWARD, 180,SERVO_DIR_CLOCKWISE, 90,SERVO_DIR_CLOCKWISE},
                {0,0, 0,0, 90,SERVO_DIR_ANTICLOCKWISE},
                {60,SERVO_DIR_REVERSE, 90,SERVO_DIR_ANTICLOCKWISE, 45,SERVO_DIR_CLOCKWISE},
                {30,SERVO_DIR_FORWARD, 90,SERVO_DIR_ANTICLOCKWISE, 45,SERVO_DIR_ANTICLOCKWISE},
        },
        .step_count = 4,
        //.state = PROGRAM_ACTIVE,
};

int store_motor_program(unsigned char prog_buf[], unsigned p_len)
{
    unsigned buf_id = 0;

    for(int i = 0; i < p_len; i++) {

        g_forming_program.step[i].x_distance_mm = ((float)(READ_16(prog_buf, buf_id)))/10;
        buf_id += 2;
        g_forming_program.step[i].x_direction = prog_buf[buf_id++];

        g_forming_program.step[i].y_rotate_deg = ((float)(READ_16(prog_buf, buf_id)))/10;
        buf_id += 2;
        g_forming_program.step[i].y_direction = prog_buf[buf_id++];

        g_forming_program.step[i].z_rotate_deg = ((float)(READ_16(prog_buf, buf_id)))/10;
        buf_id += 2;
        g_forming_program.step[i].z_direction = prog_buf[buf_id++];

        g_forming_program.step[i].z_speed_rpm = READ_16(prog_buf, buf_id);
        buf_id += 2;
    }
    g_forming_program.step_count = p_len;
    if(g_forming_program.step_count) {
        g_forming_program.state = PROGRAM_ACTIVE;
        return 1;
    }
    // Invalid program length
    return 0;
}
