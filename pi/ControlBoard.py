
import serial
import time
import struct
import sys
import binascii

__DEBUG__ = True

"""
Protocol:
| SOF | LEN | CMD_ID | CMD_DATA | EOF |

LEN = Number of bytes for CMD_ID and CMD_DATA
CMD_DATA is optional and depends on the CMD_ID
"""
#Protocol fields
SOF = '\xAE'
EOF = '\xEA'

#Commands
CMD_PING = '\x80'
CMD_ENABLE_CTRL_BOARD = '\x81'
CMD_DISABLE_CTRL_BOARD = '\x82'

CMD_MODE_PROGRAM = '\x83'
CMD_MODE_MPG = '\x84'
CMD_LOAD_PROGRAM = '\x85'

CMD_GET_ANGLE = '\x86'
CMD_GET_DISTANCE = '\x87'

CMD_MODE_IDLE = '\x88'
CMD_GOTO_HOME = '\x89'

CMD_SET_PARAMETER = '\x8A'

CMD_RSP_DELIMITER = '\xBF'

# Responses
RSP_PING = '\xC0'
RSP_OK = '\xC1'
RSP_ERROR = '\xC2'
RSP_UNKNOWN = '\xD0'


#Response
RSP_PING = '\xC0'
RSP_OK = '\xC1'
RSP_ERROR = '\xC2'
RSP_UNKNOWM = '\xD0'

# Set parameter sub commands
SUB_CMD_SET_X_OFFSET = '\x40'
SUB_CMD_SET_HOMING_SPEED_RPM = '\x41'
SUB_CMD_SET_JOB_SPEED = '\x42'
SUB_CMD_SET_MPG_SPEED = '\x43'
SUB_CMD_SET_MPG_MULTIPLIER = '\x44'

#Others
MAX_PACKET_LEN = (10 + (10 * 200))

class ControlBoard(object):

    # Run modes
    MODE_PROGRAM = 0x00
    MODE_MPG = 0x01
    MODE_IDLE = 0x02

    # Servo IDs
    SERVO_X = 0
    SERVO_Y = 1
    SERVO_Z = 2
    SERVO_ALL = 0xF

    def __init__(self, port = None):
        super(ControlBoard, self).__init__()

        if port:
            self.serialPort = port
        else:
            #Default
            self.serialPort = "/dev/ttyAMA0"
            #self.serialPort = "COM3"

        self.serialHandle = serial.Serial(self.serialPort, baudrate=115200)
        self.serialHandle.setWriteTimeout(2)
        self.serialHandle.setTimeout(2)
        self._h = self.serialHandle

        if __DEBUG__:
            self.serialHandle.setWriteTimeout(0.1)
            self.serialHandle.setTimeout(0.1)

    def sendCommand(self, cmd, cmdData=None):
        self._h.flushInput()
        if cmdData:
            length = 1+len(cmdData)
            self._h.write(SOF + chr(length & 0xFF) + \
                          chr((length >> 8) & 0xFF) + \
                          cmd + cmdData + EOF)
        else:
            self._h.write(SOF + chr(1) + '\x00' + cmd + EOF)

    def readResponse(self):
        sof = self._h.read()
        if sof != SOF:
            #Something wrong with packet; flush everything
            self._h.flushInput()
            return None, None
        lsb = ord(self._h.read())
        msb = ord(self._h.read())
        count = (lsb | (msb << 8))
        
        if count > MAX_PACKET_LEN:
            #More than maximum packet; may be corrupted
            self._h.flushInput()

        while count:
            data = self._h.read(count)
            count = count - len(data)

        eof = self._h.read()
        if eof != EOF:
            #EOF is not received properly
            self._h.flushInput()
            return None, None
        #Success
        if len(data) > 1:
            return data[0], data[1:]
        else:
            return data, None
        
    def ping(self):
        self.sendCommand(CMD_PING, None)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_PING):
            return True
        return False

    def enable(self):
        self.sendCommand(CMD_ENABLE_CTRL_BOARD, None)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            return True
        return False

    def disable(self):
        self.sendCommand(CMD_DISABLE_CTRL_BOARD, None)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            return True
        return False

    def homeServo(self, servo_id):
        cmdData = chr(servo_id)
        self.sendCommand(CMD_GOTO_HOME, cmdData)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            return True
        return False

    def homeServoAll(self):
        return self.homeServo(self.SERVO_ALL)

    def loadProgram(self, program):
        """Expects program to be array of 4 elements tuple"""
        if program:
            cmdData = ''
            # Encode number of steps
            p = []
            for i in range(len(program)):
                row = []
                for j in range(3):
                    value = float(program[i][j].strip())
                    row.append(abs(value))
                    row.append(0 if (value >= 0) else 1)
                # Speed as integer
                row.append(int(program[i][3]))
                p.append(row)
                
            cmdData += chr(len(p))
            for i in range(0, len(p)):
                # Encode Servo X distance (16-bit LE)
                cmdData += struct.pack("<HB", p[i][0]*10, p[i][1])
                # Encode Servo Y
                cmdData += struct.pack("<HB", p[i][2]*10, p[i][3])
                # Encode Servo Z
                cmdData += struct.pack("<HBH", p[i][4]*10, p[i][5], p[i][6])

            self.sendCommand(CMD_LOAD_PROGRAM, cmdData)
            [respCmd, respData] = self.readResponse()
            if(respCmd == RSP_OK):
                return True
            return False
        # Ignore empty program
        return True

    def setModeProgram(self):
        # First set the mode idle
        self.sendCommand(CMD_MODE_IDLE, None)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            # Now set the actual mode
            self.sendCommand(CMD_MODE_PROGRAM, None)
            [respCmd, respData] = self.readResponse()
            if (respCmd == RSP_OK):
                return True
        return False

    def setModeMPG(self, servo_id):
        # First set the mode idle
        self.sendCommand(CMD_MODE_IDLE, None)
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            time.sleep(0.1)
            # Now set the actual mode
            self.sendCommand(CMD_MODE_MPG, chr(servo_id))
            [respCmd, respData] = self.readResponse()
            if (respCmd == RSP_OK):
                return True
        return False

    def getMotorDistance(self, servo_id):
        self.sendCommand(CMD_GET_DISTANCE, chr(servo_id))
        [respCmd, respData] = self.readResponse()
        if(respCmd == RSP_OK):
            (dist, direction) = struct.unpack("<HB",respData)
            dist = float(dist)/10
            rvalue = -dist if direction else dist
            return (True, rvalue)
        return (False, None)

    def getMotorAngle(self, servo_id):
        self.sendCommand(CMD_GET_ANGLE, chr(servo_id))
        [respCmd, respData] = self.readResponse()
        if (respCmd == RSP_OK):
            (angle, direction) = struct.unpack("<HB",respData)
            angle = float(angle)/10;
            rvalue = -angle if direction else angle
            return (True, rvalue)
        return (False, None)

    def setParameter(self, subCmd, subCmdData=None):
        cmdData = ''
        cmdData += (subCmd + subCmdData)
        self.sendCommand(CMD_SET_PARAMETER, cmdData)
        [respCmd, respData] = self.readResponse()
        if respCmd == RSP_OK:
            return True
        return False

    def setXOffset(self, offset):
        subCmdData = ''
        subCmdData += struct.pack("<H",(float(offset)*10))
        return self.setParameter(SUB_CMD_SET_X_OFFSET, subCmdData)

    def setMotorHomingSpeed(self, servo_id, speed_rpm):
        subCmdData = struct.pack("<BH", int(servo_id), int(speed_rpm))
        return self.setParameter(SUB_CMD_SET_HOMING_SPEED_RPM, subCmdData)

    def setMotorJobSpeed(self, servo_id, speed):
        subCmdData = struct.pack("<BH", int(servo_id), int(speed))
        return self.setParameter(SUB_CMD_SET_JOB_SPEED, subCmdData)

    def setMpgSpeed(self, speed_rpm):
        subCmdData = struct.pack("<H", int(speed_rpm))
        return self.setParameter(SUB_CMD_SET_MPG_SPEED, subCmdData)

    def setMpgMultiplier(self, multiplier):
        subCmdData = struct.pack("<H", int(multiplier))
        return self.setParameter(SUB_CMD_SET_MPG_MULTIPLIER, subCmdData)

class DummyControlBoard(ControlBoard):

    def __init__(self, port = None):
        pass

    def sendCommand(self, cmd, cmdData=None):
        if cmd:
            print "Cmd: ", binascii.hexlify(cmd)
        else:
            print "Cmd: None"
        if cmdData:
            print "Cmd data: ",binascii.hexlify(cmdData)
        else:
            print "Cmd data: None"

    def readResponse(self):
        return RSP_OK, '\x94\x00\x01'
        #return RSP_UNKNOWM, None

    def ping(self):
        return True
    
if __name__ == "__main__":

    cb = ControlBoard()
    status = None
    tcount = 1
    """ Step = (X dist, Y angle, Z angle, Speed))"""
    tProgram = [(21, 45, 92, 10),
                (0, 0, 90, 20),
                (10, 120, 50, 10),
                (90, 80, 3, 15),
                (45, 4, 90, 100),
                (0, 3, 0, 25),
                ]
                
    if tcount:
        tcount = tcount - 1
        status = cb.ping()
        if status:
            print "Ping test - Pass"
        else:
            print "Ping test - Fail"

        status = cb.enable()
        print "Board enable test - ", "Pass" if status else "Fail"
  

        status = cb.disable()
        print "Board disable test - ", "Pass" if status else "Fail"
        cb.enable()

        # Mode setting test
        status = cb.setModeProgram()
        print "Set Program mode test - ", "Pass" if status else "Fail"

        # Set Homing test
        status = cb.homeServoAll()
        print "Servo all home test - ", "Pass" if status else "Fail"

        status = cb.homeServo(cb.SERVO_X)
        print "Servo X home test - ", "Pass" if status else "Fail"

        status = cb.homeServo(cb.SERVO_Y)
        print "Servo Y home test - ", "Pass" if status else "Fail"

        status = cb.homeServo(cb.SERVO_Z)
        print "Servo Z home test - ", "Pass" if status else "Fail"

        # Load program test
        status = cb.loadProgram(tProgram)
        print "Program load test - ", "Pass" if status else "Fail"

        # Set X offset of the program
        status = cb.setXOffset("42.6")
        print "Set X offset test - ", "Pass" if status else "Fail"

        status = cb.setModeMPG(cb.SERVO_X)
        print "Set MPG mode (Servo X) - ", "Pass" if status else "Fail"

        status = cb.setModeMPG(cb.SERVO_Y)
        print "Set MPG mode (Servo Y) - ", "Pass" if status else "Fail"

        status = cb.setModeMPG(cb.SERVO_Z)
        print "Set MPG mode (Servo Z) - ", "Pass" if status else "Fail"

        for servo_id in range(0, 3):
            print ""
            print "Test Servo %s - MPG read distance" %(servo_id)
            status = cb.setModeMPG(servo_id)
            if not status:
                print "Failed..."
                sys.exit(1)
            print "Rotate MPG and check if Servo %s rotates" %(servo_id)
            print "Ctrl-C to stop..."
            state = 1
            while state:
                try:
                    if(servo_id == cb.SERVO_X):
                        (status, dist) = cb.getMotorDistance(cb.SERVO_X)
                        if not status:
                            print "Servo %s read failed..." %(servo_id)
                            sys.exit(1)
                        sys.stdout.write("Distance: %6.2f mm \r" %(dist))
                    else:
                        (staus, angle) = cb.getMotorAngle(servo_id)
                        if not status:
                            print "Servo %s read failed..." %(servo_id)
                        sys.stdout.write("Angle: %6.2f deg \r" %(angle))
                    time.sleep(0.05)
                except KeyboardInterrupt:
                    state = 0

        print "Test completed!"

        
        
        
            
            
