import sys
import csv
from PyQt4 import QtGui, QtCore, uic
from AppMainWindow import Ui_MainWindow
from Manager import ProgramManager, ConfigManager
from ControlBoard import ControlBoard, DummyControlBoard
from array import *
import time

__DEBUG__ = True
sys.setrecursionlimit(1500)

# Defines
MIN_X_OFFSET = 0
MAX_X_OFFSET = 1000

MIN_X_HOMING_SPEED_RPM = 1
MAX_X_HOMING_SPEED_RPM = 500

MIN_Y_HOMING_SPEED_RPM = 1
MAX_Y_HOMING_SPEED_RPM = 500

MIN_Z_HOMING_SPEED_RPM = 1
MAX_Z_HOMING_SPEED_RPM = 500

MIN_X_JOB_SPEED = 0.1
MAX_X_JOB_SPEED = 50

MIN_Y_JOB_SPEED = 0.1
MAX_Y_JOB_SPEED = 360

MIN_Z_JOB_SPEED = 0.1
MAX_Z_JOB_SPEED = 360



class TimedMessageBox(QtGui.QMessageBox):
    def __init__(self, timeout, title, message):
        super(TimedMessageBox, self).__init__()
        self.timeout = timeout
        self.setWindowTitle(title)
        self.setText(message)
        self.setIcon(QtGui.QMessageBox.Information)

    def showEvent(self, event):
        QtCore.QTimer().singleShot(self.timeout*1000, self.close)
        super(TimedMessageBox, self).showEvent(event)

class MachineController(QtGui.QMainWindow, Ui_MainWindow):

    PROGRAM_HEADER = ["X (mm)", "Y (deg)", "Z (deg)", "Z speed (rpm)"]
    MAX_PROGRAM_STEPS = 100
    SERVO_X = 0
    SERVO_Y = 1
    SERVO_Z = 2
    MPG_UPDATE_INTERVAL_MS = 300
    MPG_MULTIPLIER_LIST = [
        "1x",
        "2x",
        "4x",
        "8x",
        "16x",
        "32x",
        ]
    
    def __init__(self):
        super(MachineController, self).__init__()

        # Variables to hold information
        self.currentProg = None
        self.currentProgSteps = None
        self.currentXOffset = None
        self.programList = None
        self.mpgMode = False
        self.mpgServo = self.SERVO_Z
        self.mpgUpdateTimer = QtCore.QTimer()
       
        self.setupUi(self)
        self.initBackend()
        self.initCB()
        self.initUI()
        self.show()

    def initCB(self):
        try:
            if __DEBUG__:
                self.cb = DummyControlBoard()
            else:
                self.cb = ControlBoard()
            if not self.cb.ping():
                self.alertConnectionLost()
        except Exception, err:
            raise
            self.errorConnection()

        if not self.cb.enable():
            self.alertConnectionLost()

    def alertConnectionLost(self):
        while not self.cb.ping():
            reply = QtGui.QMessageBox.critical(self, "Connection Error",
                                                "Lost connection with the Control Board!!!",
                                                QtGui.QMessageBox.Retry | QtGui.QMessageBox.Cancel)
            if reply != QtGui.QMessageBox.Retry:
                sys.exit(0)
                
        self.setCBProgramDetails()
        self.setCBConfDetails()

    def setCBConfDetails(self):
        if(not self.cb.ping()):
            self.alertConnectionLost()
        if(not self.cb.setMotorHomingSpeed(self.cb.SERVO_X, self.motorConf['x_homing_speed_rpm'])):
            self.alertConnectionLost()
        if(not self.cb.setMotorHomingSpeed(self.cb.SERVO_Y, self.motorConf['y_homing_speed_rpm'])):
            self.alertConnectionLost()
        if(not self.cb.setMotorHomingSpeed(self.cb.SERVO_Z, self.motorConf['z_homing_speed_rpm'])):
            self.alertConnectionLost()
        if(not self.cb.setMotorJobSpeed(self.cb.SERVO_X, self.motorConf['x_job_speed'])):
            self.alertConnectionLost()
        if(not self.cb.setMotorJobSpeed(self.cb.SERVO_Y, self.motorConf['y_job_speed'])):
            self.alertConnectionLost()
        if(not self.cb.setMotorJobSpeed(self.cb.SERVO_Z, self.motorConf['z_job_speed'])):
            self.alertConnectionLost()
        if(not self.cb.setMpgSpeed(self.motorConf['mpg_speed_rpm'])):
            self.alertConnectionLost()    

    def setCBProgramDetails(self):
        """Called to update program information in Control board"""
        if not self.cb.ping():
            self.alertConnectionLost()
        self.setCBMode()
        # Set program's X offset
        if not self.cb.setXOffset(self.currentXOffset):
            self.alertConnectionLost()
        # Assuming currentProgSteps will have the latest stored sequence
        if not self.cb.loadProgram(self.currentProgSteps):
            self.alertConnectionLost()

    def setCBMode(self):
        print "CB Mode"
        if self.mpgMode:
            if not self.cb.setModeMPG(self.mpgServo):
                self.alertConnectionLost()
            mpgMultiplier = str(self.cb_MpgMultiplier.currentText()).strip('x')
            if not self.cb.setMpgMultiplier(mpgMultiplier):
                self.alertConnectionLost()
            self.mpgUpdateTimer.start(self.MPG_UPDATE_INTERVAL_MS)
        else:
            # Stop MPG timer here
            self.mpgUpdateTimer.stop()
            if not self.cb.setModeProgram():
                self.alertConnectionLost()
    
    def desktopSize(self):
        frameGm = self.frameGeometry()
        geometry = QtGui.QDesktopWidget().availableGeometry()
        centerPoint = geometry.center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())
        #self.resize(geometry.width(), geometry.height())
        
        self.tab.setGeometry(geometry)

    def initUI(self):
        self.desktopSize()
        self.statusBar().showMessage("Ready")

        self.initProgramTab()
        self.initSettingsTab()

    def initBackend(self):
        self.pm = ProgramManager()
        self.cm = ConfigManager()
        self.programList = self.pm.get_list()
        if self.programList:
            self.currentProg = self.programList[0]
            self.currentProgSteps = self.pm.get_sequence(self.currentProg)
            pd = self.pm.get_info(self.currentProg)
            if pd:
                self.currentXOffset = pd.get('x_offset','0.0')
        else:
            self.currentProg = None
            self.currentProgSteps = []
            self.currentXOffset = '0.0'
        # Config
        self.motorConf = self.cm.get_motor_conf()

    def initProgramTab(self):
        # Program selection
        self.cb_ProgramList.addItems(self.programList)
        self.cb_ProgramList.currentIndexChanged['QString'].connect(self.eProgramChanged)

        self.tw_ps = self.tw_ProgramSequence
        self.tw_ps.setHorizontalHeaderLabels(self.PROGRAM_HEADER)
        self.tw_ps.cellChanged.connect(self.eProgStepItemChanged)
        # Update the program details on UI
        self.setUIProgramDetails()
        # Update control board too
        self.setCBProgramDetails()

        # Create the icons for the MPG button
        switchPixmap = QtGui.QPixmap("icons/on_off_buttons_2.jpg")
        self.mpgOnIcon = QtGui.QIcon(switchPixmap.copy(QtCore.QRect(40,70,360,120)))
        self.mpgOffIcon = QtGui.QIcon(switchPixmap.copy(QtCore.QRect(40,260,360,120)))

        # Setup image for MPG ON/OFF
        self.btn_MpgModeSelect.setIcon(self.mpgOffIcon)
        self.btn_MpgModeSelect.setIconSize(QtCore.QSize(80,30))
        self.btn_MpgModeSelect.setStyleSheet("border-radius:1px")

        # Set the supported MPG multipliers
        self.cb_MpgMultiplier.addItems(self.MPG_MULTIPLIER_LIST)
        self.cb_MpgMultiplier.currentIndexChanged['QString'].connect(self.eMpgMultiplierChanged)
        

        self.le_MpgServoX.setText("0.0")
        self.le_MpgServoY.setText("0.0")
        self.le_MpgServoZ.setText("0.0")

        # Setup timer for MPG
        self.mpgUpdateTimer.timeout.connect(self.eMpgUpdateTimeout)
    
        # Setup the callbacks
        self.btn_CancelProgram.clicked.connect(self.eProgCancelButtonClicked)
        self.btn_SaveProgram.clicked.connect(self.eProgSaveButtonClicked)
        self.btn_AddProgram.clicked.connect(self.eProgAddButtonClicked)
        self.btn_DeleteProgram.clicked.connect(self.eProgDeleteButtonClicked)
        self.btn_AddStep.clicked.connect(self.eProgAddStepButtonClicked)
        self.btn_DeleteStep.clicked.connect(self.eProgDeleteStepButtonClicked)

        # Mpg mode select and radio buttons
        self.btn_MpgModeSelect.clicked.connect(self.eMpgModeSelectButtonClicked)
        self.rbtn_ServoX.toggled.connect(self.eMpgServoXSelected)
        self.rbtn_ServoY.toggled.connect(self.eMpgServoYSelected)
        self.rbtn_ServoZ.toggled.connect(self.eMpgServoZSelected)

        # Mpg update button
        self.btn_MpgValueUpdate.clicked.connect(self.eMpgValueUpdateToProgram)

        # Homing buttons
        self.btn_HomeX.clicked.connect(self.eHomeXButtonClicked)
        self.btn_HomeY.clicked.connect(self.eHomeYButtonClicked)
        self.btn_HomeZ.clicked.connect(self.eHomeZButtonClicked)
        self.btn_HomeAll.clicked.connect(self.eHomeAllButtonClicked)

        # Check connection button
        self.btn_Check.clicked.connect(self.eCheckButtonClicked)


    def initSettingsTab(self):
        self.setCBConfDetails()

        # Setup the UI settings
        self.le_XHomingSpeed.setText(self.motorConf['x_homing_speed_rpm'])
        self.le_YHomingSpeed.setText(self.motorConf['y_homing_speed_rpm'])
        self.le_ZHomingSpeed.setText(self.motorConf['z_homing_speed_rpm'])
        self.le_XJobSpeed.setText(self.motorConf['x_job_speed'])
        self.le_YJobSpeed.setText(self.motorConf['y_job_speed'])
        self.le_ZJobSpeed.setText(self.motorConf['z_job_speed'])

        # Setup events for SAVE and CANCEL buttons
        self.btn_SaveSettings.clicked.connect(self.eSettingsSaveButtonClicked)
        self.btn_CancelSettings.clicked.connect(self.eSettingsCancelButtonClicked)
        

    def setUIProgramDetails(self):
        """Called to update the program details on UI from storage"""
        pd = self.pm.get_info(self.currentProg)
        self.currentProgSteps = self.pm.get_sequence(self.currentProg)
        if pd:
            self.le_Xoffset.setText(pd.get('x_offset', ''))
            self.updateProgramSteps(self.currentProgSteps)
            

        # Program steps
        self.programSteps = self.updateProgramSteps(self.currentProgSteps)

    def updateProgramSteps(self, prog_steps):
        if prog_steps:
            self.tw_ps.cellChanged.disconnect(self.eProgStepItemChanged)
            table = self.tw_ProgramSequence
            table.setRowCount(len(prog_steps))
            table.setColumnCount(len(self.PROGRAM_HEADER))
            for i in range(0, len(prog_steps)):
                for j in range(0, len(prog_steps[i])):
                    table.setItem(i, j, QtGui.QTableWidgetItem(prog_steps[i][j]))
            self.tw_ps.cellChanged.connect(self.eProgStepItemChanged)

    def updateProgramElement(self, row, col, element):
        self.currentProgSteps[row][col] = element
        table = self.tw_ProgramSequence
        table.setItem(row, col, QtGui.QTableWidgetItem(self.currentProgSteps[row][col]))
        
    # Event Callbacks
    def eProgramChanged(self, text):
        sender = self.sender()
        if str(text):
            self.currentProg = str(text)
            self.currentProgSteps = self.pm.get_sequence(self.currentProg)
            pd = self.pm.get_info(self.currentProg)
            self.currentXOffset = pd.get('x_offset','0.0')
            self.setUIProgramDetails()
            # Update control board too
            self.setCBProgramDetails()

    def eProgCancelButtonClicked(self):
        cancel_msg = "Are you sure to cancel changes?"
        reply = QtGui.QMessageBox.question(self, 'Warning', cancel_msg,
                                           QtGui.QMessageBox.Yes,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.setUIProgramDetails()

    def eProgSaveButtonClicked(self):
        # Assuming currentProgSteps have validated list of program steps
        # Validate the program parameters
        progParam = {}
        progParam['x_offset'] = str(self.le_Xoffset.text())

        # Validate parameters
        status = self.validateXoffset(progParam)
        if status:
            self.pm.change(self.currentProg, progParam)
            self.currentXOffset = progParam['x_offset']
            self.pm.update_sequence(self.currentProg, self.currentProgSteps)
            # Update control board too
            self.setCBProgramDetails()
            msgBox = TimedMessageBox(2, "Saving", "Please wait while saving '%s'..." %(self.currentProg))
            msgBox.exec_()
        else:
            # Invalid
            reply = QtGui.QMessageBox.critical(self, "Caution",
                        "Invalid X Offset value",
                        QtGui.QMessageBox.Ok)
            pd = self.pm.get_info(self.currentProg)
            self.le_Xoffset.setText(pd.get('x_offset', '0.0'))
        

    def eProgAddButtonClicked(self):
        # Show dialog to get program name
        text, ok = QtGui.QInputDialog.getText(self, 'Add New', 'Program:')
        if ok and text != '':
            self.pm.add(str(text), {'x_offset': '0.0'})
        else:
            return
        self.currentProg = str(text)
        self.currentProgSteps = self.pm.get_sequence(self.currentProg)
        self.programList = self.pm.get_list()
        self.cb_ProgramList.clear()
        self.cb_ProgramList.addItems(self.programList)
        index = self.cb_ProgramList.findText(text)
        if index >= 0:
            self.cb_ProgramList.setCurrentIndex(index)
        else:
            raise Exception("Error saving program")       

    def eProgDeleteButtonClicked(self):
        delete_msg = "Are you sure to delete '%s'?" %(self.currentProg)
        reply = QtGui.QMessageBox.question(self, 'Warning', delete_msg,
                                           QtGui.QMessageBox.Yes,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.pm.delete(self.currentProg)
            self.cb_ProgramList.clear()
            self.programList = self.pm.get_list()
            if self.programList:
                self.cb_ProgramList.addItems(self.programList)
                self.currentProg = self.programList[0]
                index = self.cb_ProgramList.findText(self.currentProg)
                self.cb_ProgramList.setCurrentIndex(index)
            else:
                self.currentProg = None

    def eProgAddStepButtonClicked(self):
        row_count = len(self.currentProgSteps)
        if row_count < self.MAX_PROGRAM_STEPS:    
            row_count += 1
            index = self.tw_ProgramSequence.currentRow()+1
            step = [x for x in self.currentProgSteps[index-1]]
            self.currentProgSteps.insert(index, step)
            # Update the program steps table
            self.tw_ProgramSequence.setRowCount(row_count)
            self.updateProgramSteps(self.currentProgSteps)
        else:
            # Sorry no more steps
            warning_msg = "Sorry, no more program steps!"
            reply = QtGui.QMessageBox.critical(self, "Caution", warning_msg,
                                               QtGui.QMessageBox.Ok)
            

    def eProgDeleteStepButtonClicked(self):
        table = self.tw_ProgramSequence
        row = table.currentRow()
        if row == 0 and len(self.currentProgSteps) == 1:
            # Program must have atleast one row
            invalid_msg = "Cannot delete row 0, Program must have minimum one step"
            reply = QtGui.QMessageBox.critical(self, 'Caution', invalid_msg,
                                               QtGui.QMessageBox.Ok)
        else:
            delete_msg = "Are you sure to delete row = %d?" %(row)
            reply = QtGui.QMessageBox.question(self, 'Warning', delete_msg,
                                               QtGui.QMessageBox.Yes,
                                               QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                self.currentProgSteps.pop(row)
            # Refresh the program steps UI
            self.updateProgramSteps(self.currentProgSteps)

    def eProgStepItemChanged(self, row, col):
        table = self.tw_ProgramSequence
        item = table.item(row, col)
        element = str(item.text())
        element = element.strip()
        if not self.validateColumnElement(col, element):
            # Invalid element value
            invalid_msg = "Invalid value at [%d, %d]" %(row, col)
            reply = QtGui.QMessageBox.warning(self, 'Warning', invalid_msg,
                                           QtGui.QMessageBox.Ok)
        else:
            # Good value :-)
            self.currentProgSteps[row][col] = element
        self.tw_ps.cellChanged.disconnect(self.eProgStepItemChanged)
        table.setItem(row, col, QtGui.QTableWidgetItem(self.currentProgSteps[row][col]))
        self.tw_ps.cellChanged.connect(self.eProgStepItemChanged)

    def eSettingsSaveButtonClicked(self):
        conf = {}
        conf['x_homing_speed_rpm'] = str(self.le_XHomingSpeed.text())
        conf['y_homing_speed_rpm'] = str(self.le_YHomingSpeed.text())
        conf['z_homing_speed_rpm'] = str(self.le_ZHomingSpeed.text())
        conf['x_job_speed'] = str(self.le_XJobSpeed.text())
        conf['y_job_speed'] = str(self.le_YJobSpeed.text())
        conf['z_job_speed'] = str(self.le_ZJobSpeed.text())

        [status, error_desc] = self.validateConf(conf)

        if status:
            # Valid
            # Update motorConf
            self.motorConf.update(conf)
            # Save configuration
            self.cm.set_motor_conf(self.motorConf)
            # Update Control Board too
            self.setCBConfDetails()
        else:
            # Invalid
            reply = QtGui.QMessageBox.critical(self, "Invalid entry",
                                               error_desc,
                                               QtGui.QMessageBox.Ok)

    def eCheckButtonClicked(self):
        self.setCBProgramDetails()
        self.setCBConfDetails()   

    def eSettingsCancelButtonClicked(self):
        # Reset the motor config
        self.le_XHomingSpeed.setText(self.motorConf['x_homing_speed_rpm'])
        self.le_YHomingSpeed.setText(self.motorConf['y_homing_speed_rpm'])
        self.le_ZHomingSpeed.setText(self.motorConf['z_homing_speed_rpm'])
        self.le_XJobSpeed.setText(self.motorConf['x_job_speed'])
        self.le_YJobSpeed.setText(self.motorConf['y_job_speed'])
        self.le_ZJobSpeed.setText(self.motorConf['z_job_speed'])   

    def eMpgModeSelectButtonClicked(self):
        if self.btn_MpgModeSelect.isChecked():
            self.mpgMode = True
            self.btn_MpgModeSelect.setIcon(self.mpgOnIcon)
        else:
            self.mpgMode = False
            self.btn_MpgModeSelect.setIcon(self.mpgOffIcon)
        self.setCBMode()

    def eMpgMultiplierChanged(self, text):
        text = str(text)
        if text:
            if not self.cb.setMpgMultiplier(text.strip('x')):
                self.alertConnectionLost()

    def eMpgServoXSelected(self, enabled):
        if enabled:
            self.mpgServo = self.SERVO_X
            if self.mpgMode:
                self.setCBMode()
        else:
            # Servo deselected
            self.le_MpgServoX.setText("0.0")

    def eMpgServoYSelected(self, enabled):
        if enabled:
            self.mpgServo = self.SERVO_Y
            if self.mpgMode:
                self.setCBMode()
        else:
            # Servo deselected
            self.le_MpgServoY.setText("0.0")

    def eMpgServoZSelected(self, enabled):
        if enabled:
            self.mpgServo = self.SERVO_Z
            if self.mpgMode:
                self.setCBMode()
        self.le_MpgServoZ.setText("0.0")

    def eMpgUpdateTimeout(self):
        if self.mpgMode:
            if self.mpgServo == self.SERVO_X:
                [status,distance] = self.cb.getMotorDistance(self.cb.SERVO_X)
                if status:
                    self.le_MpgServoX.setText(str(distance))
            elif self.mpgServo == self.SERVO_Y:
                [status,angle] = self.cb.getMotorAngle(self.cb.SERVO_Y)
                if status:
                    self.le_MpgServoY.setText(str(angle))
            elif self.mpgServo == self.SERVO_Z:
                [status,angle] = self.cb.getMotorAngle(self.cb.SERVO_Z)
                if status:
                    self.le_MpgServoZ.setText(str(angle))

    def eMpgValueUpdateToProgram(self):
        table = self.tw_ProgramSequence
        row = table.currentRow()
        col = table.currentColumn()
        if row < 0 or col < 0:
            # Invalid item selected on table
            return
        # Assuming valid row and column
        value = None
        if self.mpgServo == self.SERVO_X and col == self.SERVO_X:
            value = self.le_MpgServoX.text()
        elif self.mpgServo == self.SERVO_Y and col == self.SERVO_Y:
            value = self.le_MpgServoY.text()
        elif self.mpgServo == self.SERVO_Z and col == self.SERVO_Z:
            value = self.le_MpgServoZ.text()
        if value:
            element = float(value)
            element += float(self.currentProgSteps[row][col])
            element = str(element)
            if not self.validateColumnElement(col, element):
                # Invalid element value
                invalid_msg = "Invalid value of %s" %(element)
                reply = QtGui.QMessageBox.warning(self, 'Warning', invalid_msg,
                                               QtGui.QMessageBox.Ok)
            else:
                self.updateProgramElement(row, col, element)

    def eHomeXButtonClicked(self):
        if not self.cb.homeServo(self.cb.SERVO_X):
            self.alertConnectionLost()

    def eHomeYButtonClicked(self):
        if not self.cb.homeServo(self.cb.SERVO_Y):
            self.alertConnectionLost()

    def eHomeZButtonClicked(self):
        if not self.cb.homeServo(self.cb.SERVO_Z):
            self.alertConnectionLost()

    def eHomeAllButtonClicked(self):
        if not self.cb.homeServoAll():
            self.alertConnectionLost()

    def validateColumnElement(self, col, element):
        """Validate each element according to column type"""
        MIN_X_MM = 0
        MAX_X_MM = 1000
        MIN_DEG = 0
        MAX_DEG = 360
        MIN_SPEED_RPM = 0
        MAX_SPEED_RPM = 2000
        
        if col == 0: # (X distance in mm)
            try:
                element = float(element)
                if element < MIN_X_MM or element > MAX_X_MM:
                    raise
            except Exception, err:
                return False
            return True
        elif col in [1, 2]: # (Y and Z degree)
            try:
                element = float(element)
                if element < MIN_DEG or element > MAX_DEG:
                    raise
            except Exception, err:
                return False
            return True
        elif col == 3: # (Z speed in rpm)
            try:
                element = int(element)
                if element < MIN_SPEED_RPM or element > MAX_SPEED_RPM:
                    raise
            except Exception, err:
                return False
            return True
        # Invalid column
        return None
        
    def validateXoffset(self, param):
        element = 0
        try:
            element = float(param['x_offset'])
            if element < MIN_X_OFFSET or element > MAX_X_OFFSET:
                raise
            return True
        except Exception, err:
            return False

    def validateConf(self, conf):
        # Check all configuration parameters
        try:
            map(int, conf.values())
        except Exception, err:
            return [False, "Value must be integer"]
        
        # Validate Homing speeds
        speed = int(conf['x_homing_speed_rpm'])
        if (speed < MIN_X_HOMING_SPEED_RPM) or (speed > MAX_X_HOMING_SPEED_RPM):
            return [False, "X homing speed is out of limits"]
        speed = int(conf['y_homing_speed_rpm'])
        if (speed < MIN_Y_HOMING_SPEED_RPM) or (speed > MAX_Y_HOMING_SPEED_RPM):
            return [False, "Y homing speed is out of limits"]
        speed = int(conf['z_homing_speed_rpm'])
        if (speed < MIN_Z_HOMING_SPEED_RPM) or (speed > MAX_Z_HOMING_SPEED_RPM):
            return [False, "Z homing speed is out of limits"]
        # Validate Job speeds
        speed = int(conf['x_job_speed'])
        if (speed < MIN_X_JOB_SPEED) or (speed > MAX_X_JOB_SPEED):
            return [False, "X job speed is out of limits"]
        speed = int(conf['y_job_speed'])
        if (speed < MIN_Y_JOB_SPEED) or (speed > MAX_Y_JOB_SPEED):
            return [False, "Y job speed is out of limits"]
        speed = int(conf['z_job_speed'])
        if (speed < MIN_Z_JOB_SPEED) or (speed > MAX_Z_JOB_SPEED):
            return [False, "Z job speed is out of limits"]

        # all checked
        return [True, None]

        
def main():

    app = QtGui.QApplication(sys.argv)
    controller = MachineController()

    controller.show()

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
