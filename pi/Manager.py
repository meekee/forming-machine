import ConfigParser
import os
import shutil
import csv

class ConfigManager(object):
    
    def __init__(self, filename=None):
        if filename:
            self.filename = filename
        else:
            self.filename = 'configs.db'
        if not os.path.isfile(self.filename):
            print ("Error: %s file not found!" % self.filename)
            return None
            
        self.config = ConfigParser.ConfigParser(allow_no_value = True)
        self.config.read(self.filename)
          
    def get_motor_conf(self):
        return dict(self.config.items('motor'))
    
    def set_motor_conf(self, value_dict):
        for key in value_dict:
            self.config.set('motor', key, value_dict[key])
        self.store()
            
    def store(self):
        cfgfile = open(self.filename, 'w')
        self.config.write(cfgfile)
        cfgfile.close()
            
    def backup(self):
        shutil.copyfile(self.filename, "bk_%s" % (self.filename))
            
    def __del__(self):
        del self.config
            
class ProgramManager(object):

    PROGRAM_HEADER = ["X_distance_mm", "Y_rotation_deg",
                      "Z_rotation_deg", "Z_speed_rpm"]
    PROGRAM_INIT = ["0.0", "0.0", "0.0", "0"]

    def __init__(self, filename=None):
        if filename:
            self.filename = filename
        else:
            self.filename = 'programs.db'

        if not os.path.isfile(self.filename):
            print "Error: %s file not found!" % self.filename
            return None
        
        # open file and read all programs
        self.config = ConfigParser.ConfigParser(allow_no_value = True)
        self.config.read(self.filename)
        self.program_filename = None

        
    def get_list(self):
        return self.config.sections()

    def add(self, prog_name, value_dict):
        if prog_name not in self.config.sections():
            self.config.add_section(prog_name)
            self.program_filename = ('_'.join(map(str.lower, prog_name.split())) + '.csv')
            self.config.set(prog_name, 'filename', self.program_filename)
            for key in value_dict:
                self.config.set(prog_name, key, value_dict[key])
            self.store_new()
            return True
        return False
    
    def get_info(self, prog_name):
        programs = self.config.sections()
        if prog_name in programs:
            return dict(self.config.items(prog_name))

    def get_sequence(self, prog_name):
        if prog_name in self.config.sections():
            self.program_filename = self.config.get(prog_name, 'filename')
            progfile = open(self.program_filename,"rb")
            reader = csv.reader(progfile)
            reader.next()
            seq = []
            for row in reader:
                seq.append(row)
            return seq
        return []
        
    def append_sequence(self, prog_name, sequence):
        if prog_name in self.config.sections():
            self.program_filename = self.config.get(prog_name, 'filename')
            progfile = open(self.program_filename,"ab")
            writer = csv.writer(progfile)
            writer.writerow(sequence)
            progfile.close()
            return True
        return False

    def update_sequence(self, prog_name, whole_sequence):
        if prog_name in self.config.sections():
            self.program_filename = self.config.get(prog_name, 'filename')
            progfile = open(self.program_filename,"wb")
            writer = csv.writer(progfile)
            writer.writerow(self.PROGRAM_HEADER)
            for row in whole_sequence:
                writer.writerow(row)
            progfile.close()
        
    def delete(self, prog_name):
        if prog_name in self.config.sections():
            self.program_filename = self.config.get(prog_name, "filename")
            if os.path.isfile(self.program_filename):
                os.remove(self.program_filename)
        self.config.remove_section(prog_name)
        self.store()

    def change(self, prog_name, value_dict):
        programs = self.config.sections()
        if prog_name not in programs:
            raise Exception("Program not found!")
        for key in value_dict:
            self.config.set(prog_name, key, value_dict[key])
        self.store()

    def store_new(self):
        # Write them in the config file
        cfgfile = open(self.filename, 'w')
        self.config.write(cfgfile)
        cfgfile.close()
        
        progfile = open(self.program_filename, 'wb')
        writer = csv.writer(progfile)
        writer.writerow(self.PROGRAM_HEADER)
        writer.writerow(self.PROGRAM_INIT)
        progfile.close()

    def store(self):
        # Write them in the config file
        cfgfile = open(self.filename, 'w')
        self.config.write(cfgfile)
        cfgfile.close()
        
    def __del__(self):
        del self.config
            
            
if __name__ == '__main__':
##    cm = ConfigManager()
##    print (cm.getoffsetconf())
##    print (cm.gethmspeedconf())
##    print (cm.getjogspeedconf())
##    print (cm.getjobspeedconf())  
##    cm.setoffsetconf({'xoff':'', 'yoff':'', 'zoff':''})
##    cm.sethmspeedconf({'xhmspeed':'', 'yhmspeed':'', 'zhmspeed':''})
##    cm.setjogspeedconf({'xjogspeed':'', 'yjogspeed':'', 'zjogspeed':''})
##    cm.setjobspeedconf({'xjobspeed':'', 'yjobspeed':'', 'zjobspeed':''})
    pm = ProgramManager()
    print pm.get_list()
    pm.add("Test Program 1")
    pm.add("Test Program Extended 1")
    pm.append_sequence("Test Program 1", [1,-2.5,80.1,20])
